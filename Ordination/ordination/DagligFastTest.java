package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

public class DagligFastTest {

	private Laegemiddel l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	private DagligFast f;
	private LocalTime morgen = LocalTime.of(6, 0);
	private LocalTime middag = LocalTime.of(12, 0);
	private LocalTime aften = LocalTime.of(18, 0);
	private LocalTime nat = LocalTime.of(0, 0);
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSamletDosis() {
		DagligFast f1 = new DagligFast(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 04), l1);
		DagligFast f2 = new DagligFast(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 04), l1);
		

		
		f1.opretDosis(morgen, 1);
		f1.opretDosis(middag, 3);
		f1.opretDosis(aften, 1);
		f1.opretDosis(nat, 3);
		
		f2.opretDosis(morgen, 0);
		f2.opretDosis(middag, 0);
		f2.opretDosis(aften, 0);
		f2.opretDosis(nat, 3);
		
		assertEquals(16, f1.samletDosis(), 0.0001);
		assertEquals(6, f2.samletDosis(), 0.0001);
		
		
		
	}
	
	@Test
	public void testUgyldigSamletDosis()
	{
		DagligFast f3 = new DagligFast(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 04), l1);

		try
		{
		f3.opretDosis(morgen, -2);
		fail();
		}
		catch(IllegalArgumentException e)
		{
		assertEquals(e.getMessage(), "Skal være over 0");
		}
		
		
	}

	@Test
	public void testDoegnDosis() {
		DagligFast f1 = new DagligFast(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 10), l1);
		DagligFast f2 = new DagligFast(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 7), l1);


		
		f1.opretDosis(morgen, 3);
		f1.opretDosis(middag, 2);
		
		f2.opretDosis(aften, 5);
		f2.opretDosis(nat, 4);
		
		
	
		assertEquals(5, f1.doegnDosis(), 0.001);
		assertEquals(9, f2.doegnDosis(), 0.001);
		
	}

	// Tester for om objektet bliver skabt
	@Test
	public void testDagligFast() {
		DagligFast f1 = new DagligFast(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 04), l1);
		
		assertNotNull(f1);
	}

	
	@Test
	public void testOpretDosis() {
		
		f = new DagligFast(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 04), l1);
		
		f.opretDosis(morgen, 2);
		f.opretDosis(middag, 4);
		f.opretDosis(aften, 1);
		f.opretDosis(nat, 5);
		
		
		assertEquals(2,f.getDoser()[0].getAntal(), 0.000001);
		assertEquals(4,f.getDoser()[1].getAntal(), 0.000001);
		assertEquals(1,f.getDoser()[2].getAntal(), 0.000001);
		assertEquals(5,f.getDoser()[3].getAntal(), 0.000001);
		
		
	}
	@Test
	public void testIkkeGyldigOpretDosis() {
		
		f = new DagligFast(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 04), l1);
		
		
		try
		{
			f.opretDosis(morgen, -1);
			fail();
		
		}
		catch(IllegalArgumentException e)
		{
			assertEquals(e.getMessage(), "Skal være over 0");
		}
		
		
	}

}
