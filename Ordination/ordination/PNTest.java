package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class PNTest {
	private PN pn = new PN(null, null, null, 0);
	
	LocalDate dato = LocalDate.of(2020, 03, 5);

	

	private Laegemiddel lm1 = new Laegemiddel("Fucidin", 1, 1, 1, "Styk");

	private PN pnGD = new PN(LocalDate.of(2020, 03, 05), LocalDate.of(2020, 03, 10), lm1, 5);
	
	LocalDate TC15 = LocalDate.of(2020, 03, 5);
	LocalDate TC16 = LocalDate.of(2020, 03, 10);
	LocalDate TC17 = LocalDate.of(2020, 03, 8);

	LocalDate TC18 = LocalDate.of(2020, 03, 20);

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testPN() {
		PN pn1 = new PN(dato, dato, lm1, 5);
		assertNotNull(pn1);
	}

	@Test
	public void testGivDosis() {
		assertTrue(pnGD.givDosis(TC15));
		assertEquals(LocalDate.of(2020, 03, 05), pnGD.getDoser().get(0));
		assertTrue(pnGD.givDosis(TC16));
		assertEquals(LocalDate.of(2020, 03, 10), pnGD.getDoser().get(1));
		assertTrue(pnGD.givDosis(TC17));
		assertEquals(LocalDate.of(2020, 03, 8), pnGD.getDoser().get(2));
		assertTrue(pnGD.getDoser().size() == 3);
		assertFalse(pnGD.givDosis(TC18));
		assertTrue(pnGD.getDoser().size() == 3);
		

	}
	@Test
	public void testDoegnDosis() {
		pnGD.givDosis(TC15);
		assertEquals(5,pnGD.doegnDosis(),0.001);
		pnGD.givDosis(TC17);
		assertEquals(2.5, pnGD.doegnDosis(),0.001);
	}

}
