	package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	private ArrayList<Dosis> doser = new ArrayList<Dosis>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis d1 = new Dosis(tid, antal);
		doser.add(d1);

	}

	@Override
	public double samletDosis() {
		double samlet = 0;
		for (Dosis dos : doser) {
			samlet += dos.getAntal();
		}
		if(samlet < 0) {
			throw new RuntimeException("Samlet dosis er negativ!");
		} else {
			return samlet;
		}
	}

	@Override
	public double doegnDosis() {
		long daysBetween = ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) + 1;

		return samletDosis() / daysBetween;
	}
	
	public ArrayList<Dosis> getDoser()
	{
		return new ArrayList<>(doser);
	}

	@Override
	public String getType() {
		return "DagligSkaev";
	}
}
