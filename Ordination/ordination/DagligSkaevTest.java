package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {

	private LocalDate dato;
	private Laegemiddel lm;
	private DagligSkaev tc19;
	private DagligSkaev tc20;
	private DagligSkaev tc21;
	private LocalTime tid = LocalTime.of(16, 00);
	private LocalTime tid2 = LocalTime.of(7, 30);
	private LocalTime tid3 = LocalTime.of(9, 30);
	private LocalTime tid4 = LocalTime.of(12, 30);

	@Before
	public void setUp() throws Exception {
		dato = LocalDate.of(2020, 03, 04);
		lm = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		tc19 = new DagligSkaev(dato, dato, lm);
		tc20 = new DagligSkaev(dato, dato, lm);
		tc21 = new DagligSkaev(dato, dato, lm);
	}

	@Test
	public void testDagligSkaev() {
		DagligSkaev ds1 = new DagligSkaev(dato, dato, lm);
		assertNotNull(ds1);
	}

	@Test
	public void testSamletDosis() {

		tc19.opretDosis(tid, 1);
		tc19.opretDosis(tid2, 3);
		tc19.opretDosis(tid3, 1);
		tc19.opretDosis(tid4, 3);
		assertEquals(tc19.samletDosis(), 8, 0.1);

		tc20.opretDosis(tid, 3);
		assertEquals(tc20.samletDosis(), 3, 0.1);
	}

	@Test
	public void testSamletDosisFejlVedNegativ() {
		tc21.opretDosis(tid, -2);
		try {
			tc21.samletDosis();
			fail();
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Samlet dosis er negativ!");
		}
	}

	@Test
	public void testOpretDosis() {
		
		// Indsatte doser:
		tc19.opretDosis(tid, 4);
		
		assertEquals(4, tc19.getDoser().get(0).getAntal(), 0.01);
	}

	@Test
	public void testDoegnDosis() {
		
		tc19.opretDosis(tid, 2);
		tc19.opretDosis(tid2, 3);
		
		tc20.opretDosis(tid, 2);
		
		assertEquals(5, tc19.doegnDosis(), 0.01);
		assertEquals(2, tc20.doegnDosis(), 0.01);
	}
}
