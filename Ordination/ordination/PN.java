package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> doser = new ArrayList<>();
	private int antalGangeGivet = 0;

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder) {

		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean givet = false;
		if (givesDen.equals(getStartDen()) || givesDen.equals(getSlutDen())) {
			givet = true;
			doser.add(givesDen);
			antalGangeGivet++;

		} else {
			if (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen())) {
				givet = true;
				doser.add(givesDen);
				antalGangeGivet++;

			}

		}
		return givet;
	}

	public double doegnDosis() {
		return (samletDosis()) / (ChronoUnit.DAYS.between(getFoersteGivning(), getSidsteGivning()) + 1);
		// return (doser.size() > 0 ? samletDosis() /
		// (ChronoUnit.DAYS.between(Collections.min(doser), Collections.max(doser))) :
		// samletDosis());
	}

	public LocalDate getFoersteGivning() {
		LocalDate foersteGivning = getSlutDen();
		for (LocalDate ld : doser) {
			if (ld.isEqual(getStartDen())) {
				return ld;
			} else if (ld.isBefore(foersteGivning)) {
				foersteGivning = ld;
			}
		}
		return foersteGivning;
	}

	public LocalDate getSidsteGivning() {
		LocalDate sidsteGivning = getStartDen();
		for (LocalDate ld : doser) {
			if (ld.isEqual(getSlutDen())) {
				return ld;
			} else if (ld.isAfter(sidsteGivning)) {
				sidsteGivning = ld;
			}
		}
		return sidsteGivning;
	}

	public double samletDosis() {
		return antalGangeGivet * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return antalGangeGivet;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

	public ArrayList<LocalDate> getDoser() {
		return doser;
	}

}
