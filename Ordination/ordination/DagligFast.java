package ordination;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class DagligFast extends Ordination {

	private Dosis[] doser = new Dosis[4];
	private LocalTime morgen = LocalTime.of(6, 0);
	private LocalTime middag = LocalTime.of(12, 0);
	private LocalTime aften = LocalTime.of(18, 0);
	private LocalTime nat = LocalTime.of(0, 0);
	// - rettelse

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
	}



	public void opretDosis(LocalTime time, double antal) {

		Dosis d1 = new Dosis(time, antal);
		for (int i = 0; i < doser.length; i++) {
			if (d1.getAntal() >= 0) {
				if (time.equals(morgen)) {
					doser[0] = d1;
				} else if (time.equals(middag)) {
					doser[1] = d1;
				} else if (time.equals(aften)) {
					doser[2] = d1;
				} else if (time.equals(nat)) {
					doser[3] = d1;
				}
			} else {
				throw new IllegalArgumentException("Skal være over 0");
			}
		}
	}

	public Dosis[] getDoser() {
		return doser;
	}

	@Override
	public double samletDosis() {
		double sd = 0;
		long dayBetween = ChronoUnit.DAYS.between(getStartDen(), getSlutDen())+1;
		for (int i = 0; i < doser.length; i++) {
			if (doser[i] != null) {
				sd += doser[i].getAntal()*dayBetween;
			}

		}

		return sd;
	}

	@Override
	public double doegnDosis() {

		long dayBetween = ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) + 1;

		return samletDosis() / dayBetween;
	}

	@Override
	public String getType() {
		return "DagligFast";
	}

	// sup fams

	// TODO
}
