package controller;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.PN;

import ordination.Patient;

public class ControllerTest {

	private Laegemiddel l1, l2, l3, l4, l5;
	private DagligFast d1;
	private Controller c1, c2;
	private Patient p1, p2, p3, p4, p5;
	private Laegemiddel lm = new Laegemiddel("Fucidin", 1, 1, 1, "Styk");
	private LocalDate startDen = LocalDate.of(2020, 03, 05);
	private LocalDate slutDen = LocalDate.of(2020, 03, 10);
	private PN pn1 = new PN(LocalDate.of(2020, 03, 05), LocalDate.of(2020, 03, 10), lm, 5);
	private Laegemiddel lm2;
	private LocalDate dato1;
	private LocalDate dato2;
	private int vaegt1;
	private int vaegt2;

	private Patient p6 = new Patient("3001951324", "Knud Svagarm", 100);

	@Before
	public void setUp() throws Exception {
		c1 = Controller.getController();
		c2 = Controller.getController();

	}

	@Test
	public void testOpretPNOrdination() {
		pn1 = c1.opretPNOrdination(startDen, slutDen, p6, lm, 10);
		assertNotNull(pn1);
		assertEquals(pn1, p6.getOrdinationer().get(0));

		LocalDate pn2Dato = LocalDate.of(2020, 03, 03);
		PN pn2 = c1.opretPNOrdination(pn2Dato, pn2Dato, p6, lm, 10);
		assertNotNull(pn2);
		assertEquals(pn2, p6.getOrdinationer().get(1));

	}

	public void testOpretDagligFastOrdination() {

		p1 = c1.opretPatient("121256-0512", "Jane Jensen", 63.4);
		l1 = c1.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

		c1.opretDagligFastOrdination(LocalDate.of(2020, 03, 01), LocalDate.of(2020, 03, 03), p1, l1, 0, 0, 4, 0);

		p2 = c1.opretPatient("121256-0512", "Benny Jensen", 63.4);
		l2 = c1.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

		c2.opretDagligFastOrdination(LocalDate.of(2020, 03, 01), LocalDate.of(2020, 03, 03), p1, l1, -2, -1, -1, 0);

		// Tester om den bliver lavet og har korrekt antal
		assertNotNull(p1.getOrdinationer());
		assertEquals(12, p1.getOrdinationer().get(0).samletDosis(), 0.001);
		// Tester for hvis man ligger minus i, om de bliver added
		assertEquals(0, p2.getOrdinationer().size(), 0.001);
	}

	@Test
	public void testUgyldigOpretDagligFastOrdination() {

		p1 = c1.opretPatient("121256-0512", "Jane Jensen", 63.4);
		l1 = c1.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		try {
			c1.opretDagligFastOrdination(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 01), p1, l1, 0, 0, 4, 0);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Startdato er efter slutdato");
		}

	}

	@Test
	public void testOpretDagligSkaevOrdination() {

		p1 = c1.opretPatient("121256-0512", "Jane Jensen", 63.4);
		l1 = c1.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		l2 = c1.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		l3 = c1.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		l4 = c1.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		LocalTime[] kl = { LocalTime.of(16, 0), LocalTime.of(12, 40) };
		double[] an = { 2, 1 };

		c1.opretDagligSkaevOrdination(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 05), p1, l3, kl, an);

		assertNotNull(p1.getOrdinationer());
	}

	@Test
	public void testUgyldigOpretDagligSkaevOrdination() {

		p1 = c1.opretPatient("121256-0512", "Jane Jensen", 63.4);
		l1 = c1.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		l2 = c1.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		l3 = c1.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		l4 = c1.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		LocalTime[] kl = { LocalTime.of(16, 0), LocalTime.of(12, 40) };
		double[] an = { 2 };
		try {
			c1.opretDagligSkaevOrdination(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 05), p1, l3, kl, an);
		}

		catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "De er ikke ens antal og klok");
		}
	}

	@Test
	public void testUgyldig2OpretDagligSkaevOrdination() {

		p1 = c1.opretPatient("121256-0512", "Jane Jensen", 63.4);
		l1 = c1.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		l2 = c1.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		l3 = c1.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		l4 = c1.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		LocalTime[] kl = { LocalTime.of(16, 0), LocalTime.of(12, 40) };
		double[] an = { 2 };
		try {
			c1.opretDagligSkaevOrdination(LocalDate.of(2020, 03, 05), LocalDate.of(2020, 03, 03), p1, l3, kl, an);
		}

		catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "sluttid er før starttid");

		}
	}

	public void testOpretPNOrdinationFejl() {
		try {
			PN pn3 = c1.opretPNOrdination(slutDen, startDen, p6, lm, 10);
			fail();
		} catch (IllegalArgumentException ie) {
			ie = new IllegalArgumentException("sluttid er før starttid");

		}
	}

	@Test
	public void testOrdinationPNAnvendt() {
		LocalDate dato = LocalDate.of(2020, 03, 8);
		c1.ordinationPNAnvendt(pn1, dato);
		assertEquals(LocalDate.of(2020, 03, 8), pn1.getDoser().get(0));
		LocalDate dato1 = LocalDate.of(2020, 03, 10);
		c1.ordinationPNAnvendt(pn1, dato1);
		assertEquals(LocalDate.of(2020, 03, 10), pn1.getDoser().get(1));
		LocalDate dato2 = LocalDate.of(2020, 03, 05);
		c1.ordinationPNAnvendt(pn1, dato2);
		assertEquals(LocalDate.of(2020, 03, 05), pn1.getDoser().get(2));

	}

	@Test
	public void testOrdinationPNAnvendtFejl() {
		LocalDate dato3 = LocalDate.of(2020, 03, 02);
		try {
			c1.ordinationPNAnvendt(pn1, dato3);
			fail();
		} catch (IllegalArgumentException ie) {
			ie = new IllegalArgumentException("No bueno, junkie.");
		}
	}

	@Test
	public void testAnbefaletDosisPrDoegn() {
		lm2 = c1.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		p5 = c1.opretPatient("220808-2121", "Frederik", 24);
		p4 = c1.opretPatient("071200-4578", "Anders", 80);
		p3 = c1.opretPatient("260387-4651", "Rune", 130);

		assertEquals(0.6, c1.anbefaletDosisPrDoegn(p5, lm2), 0.001);
		assertEquals(2.0, c1.anbefaletDosisPrDoegn(p4, lm2), 0.001);
		assertEquals(3.25, c1.anbefaletDosisPrDoegn(p3, lm2), 0.001);

	}

	@Test
	public void testAntalOrdinationerPrVaegtPrLaegemiddel() {
		dato1 = LocalDate.of(2020, 03, 03);
		dato2 = LocalDate.of(2020, 03, 05);
		vaegt1 = 25;
		vaegt2 = 70;
		p5 = c1.opretPatient("220808-2121", "Frederik", 50);
		lm2 = c1.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		c1.opretPNOrdination(dato1, dato2, p5, lm2, 2);
		assertEquals(1, c1.antalOrdinationerPrVaegtPrLaegemiddel(vaegt1, vaegt2, lm2));
	}

	@Test
	public void testOpretPatient() {
		p5 = c1.opretPatient("220808-2121", "Frederik Holmboe", 90);
		assertNotNull(p5);
	}

	@Test
	public void testOpretLaegemiddel() {
		lm2 = c1.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		assertNotNull(lm2);
	}

}
